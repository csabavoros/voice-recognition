import speech_recognition as sr
from time import ctime
import time
import playsound
import os
import random
from gtts import *
import webbrowser

r = sr.Recognizer()


def record_audio(ask=False):
    with sr.Microphone() as source:
        if ask:
            print(ask)
        audio = r.listen(source)
        voice_data = ''
        try:
            voice_data = r.recognize_google(audio, language='hu-HU')
        except sr.UnknownValueError:
            robot_speak('Bocsi, nem értettem')
        except sr.RequestError:
            robot_speak('Bocsi, a szolgáltató nem elérhető')
        return voice_data


def robot_speak(audio_str):
    tts = gTTS(text=audio_str, lang='hu')
    r = random.randint(1, 10000000)
    audio_file = 'audio-' + str(r) + '.mp3'
    tts.save(audio_file)
    playsound.playsound(audio_file)
    print(audio_str)
    os.remove(audio_file)


def respond(voice_data):
    if 'mi a neved' in voice_data or 'Mi a neved' in voice_data:
        robot_speak('A nevem Jarvis')
    if 'mennyi az idő' in voice_data or 'Mennyi az idő' in voice_data:
        robot_speak(time.strftime('%Y %b %d %H:%M'))
    if 'keresés' in voice_data:
        search = record_audio('Mit szeretnél keresni?')
        url = 'https://google.com/search?q=' + search
        webbrowser.get().open(url)
        robot_speak('Ez az amit találtam: ' + search)
    if 'helyszín' in voice_data:
        location = record_audio('Hova szeretnél menni?')
        url = 'https://google.nl/maps/place/' + location + '/&amp;'
        webbrowser.get().open(url)
        robot_speak('Itt a helyszín: ' + location)
    if 'kilépés' in voice_data or 'Kilépés' in voice_data:
        robot_speak('Szép napot')
        exit()


time.sleep(1)
robot_speak('Mit szeretnél?')
while 1:
    voice_data = record_audio()
    print(voice_data)
    respond(voice_data)
